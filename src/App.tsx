import React from 'react';
import { useMutation } from 'react-query';
import axios from 'axios';
import { Button, Layout, Form, Input, Divider, } from 'antd';
import 'antd/dist/antd.css';
import { Content, Header } from 'antd/lib/layout/layout';
import styles from './App.module.css'

const App = () => {
  const mutation = useMutation((payload: { num1: number; num2: number }) => axios.post(`https://calculator-dev-1.herokuapp.com/calculator`, payload));

  // antd
  const onFinish = (values: any) => {
    // console.log('Success:', values);
    mutation.mutate({ num1: values.num1, num2: values.num2 });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <Layout className={styles.layout}>
        <Header className={styles.header}>
          <span>calculator</span>
        </Header>
        <Content className={styles.contentWrapper}>
          <div className={styles.card}>
            {/* server errors */}
            {mutation.isError && <div>Error: {(mutation.error as any).message}</div>}

            <Form
              name="calculator"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              className={styles.form}
            >
              <div className={styles.text}>
                <span>Enter the numbers</span>
              </div>
              <Form.Item
                name="num1"
                rules={[{ required: true, message: 'Please input the first number!' }]}
              >
                <Input placeholder="number 1" className={styles.inputValue} />
              </Form.Item>

              <Form.Item
                name="num2"
                rules={[{ required: true, message: 'Please input the second number' }]}
              >
                <Input placeholder="number 2" className={styles.inputValue} />
              </Form.Item>

              <Form.Item>

                <Button className={styles.btnSum} htmlType="submit">
                  {mutation.isLoading ? "Adding..." : "Sum"}
                </Button>
              </Form.Item>
            </Form>

            <Divider className={styles.divider} />

            <div className={styles.text}>
              <span>Results</span>
            </div>

            <div className={styles.resultWrapper}>
              <Input
                className={styles.result}
                disabled
                value={mutation.data?.data.data.sum}
              />
            </div>

          </div>
        </Content>
      </Layout>
    </div>
  );
}

export default App;
